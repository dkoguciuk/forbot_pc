#include "ros/ros.h"
#include "forbot_pc/SwitchByte.h"


void myCallback(const forbot_pc::SwitchByte::ConstPtr& msg)
{
  	ROS_INFO("Stan przyciskow: [%i]", msg->switch_byte);
}

int main(int argc, char **argv)
{
	//Inicjacja w systemie ROS
	ros::init(argc, argv, "switch_subscriber");

	//Uchwyt do node'a
	ros::NodeHandle n;

	//Subscriber
	ros::Subscriber switch_subscriber = n.subscribe("switch_state", 1000, myCallback);

	//Spinujemy
	ros::spin();

	//Return
	return 0;
}


